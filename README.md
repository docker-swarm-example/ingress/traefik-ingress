# Traefik

Traefik is a HTTP reverse proxy and load balancer for Docker swarm. Read more about Traefik [here](https://docs.traefik.io).

## Deploy

Execute the following commands to deploy Traefik to Docker swarm:

```sh
git clone https://gitlab.com/docker-swarm-example/ingress/traefik-ingress
docker stack deploy --compose-file traefik-ingress/docker-compose.yml ingress
```

## Forward port 80 to Traefik

If you are running Docker swarm inside Virtualbox and want to be able to reach Traefik from outside of the VM you have to forward port 80. To do this you have to do the following: 

- Open "Oracle VM Virtualbox Manager" (usually located at "C:\Program Files\Oracle\VirtualBox\VirtualBox.exe")
- Right click on your docker machine (usually named "default")
- Click on "Settings"
- Go to "Network" and expand "Advanced"
- Click on "Port Forwarding"
- Click on the plus sign
- Add a new rule and set following fields to:

|Name|Value|
|---|---|
|Name|http|
|Protocol|TCP|
|Host IP|0.0.0.0|
|Host Port|80|
|Guest IP|   |
|Guest Port|80|

Now you should be able to use your web browser and browse to http://localhost/traefik to see your Traefik dashboard. If you want to reach traefik from the internet you will also have to make a port forwarding in your router.

